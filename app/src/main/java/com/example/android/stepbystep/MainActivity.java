package com.example.android.stepbystep;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.example.android.stepbystep.R.id.recommendStudy_textview;

public class MainActivity extends AppCompatActivity {

    private View preQuestionView;
    private View firstQuestionView;
    private View secondQuestionView;
    private View thirdQuestionView;
    TextView result_TextView;

    private EditText userNameInput;
    private String userNameString;

    private int result;
    //Context context = getApplicationContext();
    private String userName;
    private String language;
    String resultText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * Button result to Yes
      * @param view is from activity_main.xml
     */
    public void answerIsYes(View view){
        result = 1;
        resultOfPreQuestion(view);
    }

    /**
     * Button result to No
     * @param view is from activity_main.xml
     */
    public void answerIsNo(View view){
        result = 0;
        resultOfPreQuestion(view);
    }

    /**
     * Reset data and result of button and radio group
     * @param view is from activity_main.xml
     */
    public void resetButton(View view){
        preQuestionView = findViewById(R.id.start);
        preQuestionView.setVisibility(VISIBLE);

        firstQuestionView.setVisibility(GONE);
        secondQuestionView.setVisibility(GONE);
        thirdQuestionView.setVisibility(GONE);
        result_TextView.setVisibility(GONE);

        userNameInput.setText("");
        userNameString = "";

        result = -1;
        userName = "";
        language = "";
        resultText = "";
    }

    public void resultOfPreQuestion(View view) {
        preQuestionView = findViewById(R.id.start);
        firstQuestionView = findViewById(R.id.Question_1);
        Context context = getApplicationContext();
        if(result > 0){
            preQuestionView.setVisibility(GONE);
            firstQuestionView.setVisibility(VISIBLE);
        }
        else if (result == -1){
            //Do nothing
        }
        else {
        Toast.makeText( context, "But...I was going to introduce a fun stuff.",
            Toast.LENGTH_SHORT).show();
        }
        //reset result to 3 for reuse
        result=-1;
    }

    /**
     * result of first question will be store and it's view's visibility will set as gone.
     * @param view
     */
    public void resultOfFirstQuestion(View view){
        firstQuestionView = findViewById(R.id.Question_1);
        secondQuestionView = findViewById(R.id.Question_2);
        userNameInput = (EditText)findViewById(R.id.userName);
        userNameString = userNameInput.getText().toString();
        userName=userNameString;

        firstQuestionView.setVisibility(GONE);
        secondQuestionView.setVisibility(VISIBLE);
    }

    public void resultOfSecondQuestion(View view){
        secondQuestionView = findViewById(R.id.Question_2);
        thirdQuestionView = findViewById(R.id.Question_3);

        //Is Done is clicked ?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.java_Button:
                if (checked)
                    language = "I see you are good at Java";
                    break;
            case R.id.cPlus_Button:
                if (checked)
                    language = "I see you are good at C++";
                    break;
            case R.id.python_Button:
                if (checked)
                    language = "I see you are good at Python";
                    break;
            case R.id.etc_Button:
                if (checked)
                    language = "Sound like a tough one you learned!";
                    break;
        }
        secondQuestionView.setVisibility(GONE);
        thirdQuestionView.setVisibility(VISIBLE);
    }

    public void resultOfThirdQuestion(View view) {
        thirdQuestionView = findViewById(R.id.Question_3);
        thirdQuestionView.setVisibility(GONE);
        resultSummary();
    }

    public void resultSummary(){
        resultText = "Hello, " + userName + " I am glad to hear that you want to learn more";
        resultText += "\n" + language;

        result_TextView = (TextView) findViewById(R.id.recommendStudy_textview);
        result_TextView.setText(resultText);
    }

}


